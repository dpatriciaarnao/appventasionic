import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
//Servicio que busca los articulos de interes de selección del usuario
export class BuscaArticulosService {

  pathArticulos: string = 'https://api.mercadolibre.com/sites/MCO/search?q=';
  apiUrl: string;

  //Servicio que busca los articulos de interes del usuario
  constructor(private http: HttpClient) { 
     // Se trae el endpoint de acuerdo al evento
     this.apiUrl = this.pathArticulos;
     console.log("este es el api", this.apiUrl);
  }

  //Busca la lista de articulos
  private ejecutarQueryArticulos(articuloIntroducido) {
    const url = this.pathArticulos+articuloIntroducido
    console.log("este es el url", url);
    return this.http.get(url);
  }

  cargarArticulos(articuloIntroducido) {
    return this.ejecutarQueryArticulos(articuloIntroducido);
  }

}
