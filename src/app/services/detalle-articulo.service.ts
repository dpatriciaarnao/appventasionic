import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
//Servicio que busca el detalle del articulo seleccionado por el usuario
export class DetalleArticuloService {

  pathDetalleArticulo: string = 'https://api.mercadolibre.com/items/';
  apiUrl: string;

  constructor(private http: HttpClient) { 
     this.apiUrl = this.pathDetalleArticulo;
     console.log("este es el api", this.apiUrl);
  }

  //Busca el detalle del articulo
  getDetalleArticulo(id) {
    const url = this.pathDetalleArticulo+id;
    console.log("este es el url 2", url);
    return this.http.get(url);
  }

  cargarDetalleArticulo(id){
    return this.getDetalleArticulo(id);
  }
}
