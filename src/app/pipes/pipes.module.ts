import { NgModule } from '@angular/core';
import { ImgUrlPipe } from './img-url.pipe';
@NgModule({
    declarations: [
        ImgUrlPipe
    ],
    imports: [],
    exports: [
        ImgUrlPipe
    ]
})
export class PipesModule {}
  