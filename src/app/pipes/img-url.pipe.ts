import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'imgUrl'
})
//Pipe que trae imagen
export class ImgUrlPipe implements PipeTransform {

  transform(img: string, size: string = 'w500'): string {

    if (!img) {
      return './assets/nodisp/gris.jpg';
    }

    const imgUrl = `${img}`;
    return imgUrl;
  }
}
