import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})

//Pantalla inicial
export class FolderPage implements OnInit {
  public folder: string;
  articulo: string;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

  //Cuando el cliente busque el artículo navegará hacia el segundo page
  buscarArticulos() {
    console.log("articulo escrito", this.articulo);
    this.router.navigate(['lista-articulos', this.articulo])
  }


}
