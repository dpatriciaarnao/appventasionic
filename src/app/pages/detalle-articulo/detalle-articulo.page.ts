import { Component, OnInit } from '@angular/core';
import { RespDetalleArticulo } from 'src/app/model/DetalleArticulo';
import { ActivatedRoute } from '@angular/router';
import { DetalleArticuloService } from 'src/app/services/detalle-articulo.service';

@Component({
  selector: 'app-detalle-articulo',
  templateUrl: './detalle-articulo.page.html',
  styleUrls: ['./detalle-articulo.page.scss'],
})
//Muestra el page con el detalle del articulo seleccionado
export class DetalleArticuloPage implements OnInit {

  public articulo: any;
  id: any;

  constructor(private detalleArticuloService: DetalleArticuloService,
    private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  async ngOnInit() {
    await this.cargarDetalleArticulo(this.id);
  }

  //Carga detalle del articulo pasandole el id del articulo
  async cargarDetalleArticulo(id) {
    await this.detalleArticuloService.cargarDetalleArticulo(id).subscribe((resp) => {
      this.articulo = resp;
      console.log('"ESTE ES EL DETALLE ARTICULO"', this.articulo);
    });
  }

}
