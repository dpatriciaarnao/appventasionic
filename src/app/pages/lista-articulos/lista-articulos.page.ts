import { Component, OnInit } from '@angular/core';
import { BuscaArticulosService } from 'src/app/services/busca-articulos.service';
import { RespArticulos } from 'src/app/model/Articulos';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-articulos',
  templateUrl: './lista-articulos.page.html',
  styleUrls: ['./lista-articulos.page.scss'],
})

//Page que muestra el listado de articulos
export class ListaArticulosPage implements OnInit {
  public articulos: RespArticulos[] = [];
  articuloIntroducido: any;

  constructor(private articulosService: BuscaArticulosService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.articuloIntroducido = this.activatedRoute.snapshot.paramMap.get('articulo');
    this.cargarArticulos(this.articuloIntroducido);
  }

  //Carga la lista de articulos
  cargarArticulos(articuloIntroducido) {
    this.articulosService.cargarArticulos(articuloIntroducido).subscribe((resp: RespArticulos[]) => {
      this.articulos = resp;
      console.log('"ESTOS SON LOS ARTICULOS"', this.articulos);
    });
  }

  //Carga el detalle del articulo
  detalleArticulo(id) {
    this.router.navigate(['detalle-articulo', id]);
  }

}
