export interface RespDetalleArticulo {
    id: string;
    site_id: string;
    title: string;
    subtitle?: any;
    seller_id: number;
    category_id: string;
    official_store_id?: any;
    price: number;
    base_price: number;
    original_price?: any;
    currency_id: string;
    initial_quantity: number;
    available_quantity: number;
    sold_quantity: number;
    sale_terms: any[];
    buying_mode: string;
    listing_type_id: string;
    start_time: string;
    stop_time: string;
    condition: string;
    permalink: string;
    thumbnail: string;
    secure_thumbnail: string;
    pictures: Picture[];
    video_id?: any;
    descriptions: Description[];
    accepts_mercadopago: boolean;
    non_mercado_pago_payment_methods: any[];
    shipping: Shipping;
    international_delivery_mode: string;
    seller_address: Selleraddress;
    seller_contact: Sellercontact;
    location: Location;
    geolocation: Geolocation;
    coverage_areas: any[];
    attributes: Attribute[];
    warnings: any[];
    listing_source: string;
    variations: any[];
    status: string;
    sub_status: any[];
    tags: any[];
    warranty?: any;
    catalog_product_id?: any;
    domain_id: string;
    parent_item_id?: any;
    differential_pricing?: any;
    deal_ids: any[];
    automatic_relist: boolean;
    date_created: string;
    last_updated: string;
    health?: any;
    catalog_listing: boolean;
  }
  
export interface Attribute {
    id: string;
    name: string;
    value_id?: string;
    value_name: string;
    value_struct?: Valuestruct;
    values: Value[];
    attribute_group_id: string;
    attribute_group_name: string;
  }
  
export interface Value {
    id?: string;
    name: string;
    struct?: Valuestruct;
  }
  
export interface Valuestruct {
    number: number;
    unit: string;
  }
  
export interface Geolocation {
    latitude: number;
    longitude: number;
  }
  
export interface Location {
    address_line: string;
    zip_code: string;
    neighborhood: City;
    city: City;
    state: City;
    country: City;
    latitude: number;
    longitude: number;
  }
  
export interface Sellercontact {
    contact: string;
    other_info: string;
    country_code: string;
    area_code: string;
    phone: string;
    country_code2: string;
    area_code2: string;
    phone2: string;
    email: string;
    webpage: string;
  }
  
export interface Selleraddress {
    city: City;
    state: City;
    country: City;
    search_location: Searchlocation;
    latitude: number;
    longitude: number;
    id: number;
  }
  
export interface Searchlocation {
    city: City;
    state: City;
  }
  
export interface City {
    id: string;
    name: string;
  }
  
export interface Shipping {
    mode: string;
    methods: any[];
    tags: any[];
    dimensions?: any;
    local_pick_up: boolean;
    free_shipping: boolean;
    logistic_type: string;
    store_pick_up: boolean;
  }
  
export interface Description {
    id: string;
  }
  
export interface Picture {
    id: string;
    url: string;
    secure_url: string;
    size: string;
    max_size: string;
    quality: string;
  }