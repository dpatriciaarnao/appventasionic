export interface RespArticulos {
    site_id: string;
    query: string;
    paging: Paging;
    results: Result[];
    secondary_results: any[];
    related_results: any[];
    sort: Country;
    available_sorts: Country[];
    filters: Filter[];
    available_filters: Availablefilter[];
  }
  
export interface Availablefilter {
    id: string;
    name: string;
    type: string;
    values: Value3[];
  }
  
export interface Value3 {
    id: string;
    name: string;
    results: number;
  }
  
export interface Filter {
    id: string;
    name: string;
    type: string;
    values: Value2[];
  }
  
export interface Value2 {
    id: string;
    name: string;
    path_from_root?: Country[];
  }
  
export interface Result {
    id: string;
    site_id: string;
    title: string;
    seller: Seller;
    price: number;
    currency_id: string;
    available_quantity: number;
    sold_quantity: number;
    buying_mode: string;
    listing_type_id: string;
    stop_time: string;
    condition: string;
    permalink: string;
    thumbnail: string;
    accepts_mercadopago: boolean;
    installments?: any;
    address: Address;
    shipping: Shipping;
    seller_address: Selleraddress;
    seller_contact: Sellercontact;
    location: Location;
    attributes: Attribute[];
    original_price?: any;
    category_id: string;
    official_store_id?: number;
    domain_id?: string;
    catalog_product_id?: any;
    tags: string[];
  }
  
export interface Attribute {
    value_name: string;
    value_struct?: Valuestruct;
    attribute_group_name: string;
    source: number;
    id: string;
    name: string;
    value_id?: string;
    values: Value[];
    attribute_group_id: string;
  }
  
export interface Value {
    id?: string;
    name: string;
    struct?: Valuestruct;
    source: number;
  }
  
export interface Valuestruct {
    number: number;
    unit: string;
  }
  
export interface Location {
    address_line: string;
    zip_code: string;
    subneighborhood?: any;
    neighborhood: Country;
    city: Country;
    state: Country;
    country: Country;
    latitude: number;
    longitude: number;
  }
  
export interface Sellercontact {
    contact: string;
    other_info: string;
    area_code: string;
    phone: string;
    area_code2: string;
    phone2: string;
    email: string;
    webpage: string;
  }
  
export interface Selleraddress {
    id: string;
    comment: string;
    address_line: string;
    zip_code: string;
    country: Country;
    state: Country;
    city: City;
    latitude: string;
    longitude: string;
  }
  
export interface City {
    id?: string;
    name: string;
  }
  
export interface Country {
    id: string;
    name: string;
  }
  
export interface Shipping {
    free_shipping: boolean;
    mode: string;
    tags: any[];
    logistic_type?: string;
    store_pick_up: boolean;
  }
  
export interface Address {
    state_id: string;
    state_name: string;
    city_id: string;
    city_name: string;
    area_code: string;
    phone1: string;
  }
  
export interface Seller {
    id: number;
    permalink: string;
    power_seller_status?: string;
    car_dealer: boolean;
    real_estate_agency: boolean;
    tags: any[];
    car_dealer_logo?: string;
  }
  
export interface Paging {
    total: number;
    offset: number;
    limit: number;
    primary_results: number;
  }